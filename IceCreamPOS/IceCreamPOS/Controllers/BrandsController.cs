﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IceCreamPOS.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IceCreamPOS.Controllers
{
    public class BrandsController : Controller
    {
        private readonly IceCreamDbContext _context;

        public BrandsController(IceCreamDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            return View(await _context.FridgeBrands.ToListAsync());
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(FridgeBrand fridgeBrand)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (_context.FridgeBrands.Any(m => m.Name == fridgeBrand.Name))
                    {
                        ModelState.AddModelError("Name", "This Name is Already Exist");
                        return View(fridgeBrand);
                    }

                    await _context.FridgeBrands.AddAsync(fridgeBrand);
                    bool status = await _context.SaveChangesAsync() > 0;
                    if (status)
                    {
                        return RedirectToAction(nameof(Index));
                    }

                }

                return View(fridgeBrand);
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }
            
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            var fridgeBrand = await _context.FridgeBrands.FindAsync(id);
            if (fridgeBrand == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }
            return View(fridgeBrand);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, FridgeBrand fridgeBrand, string editValue)
        {
            try
            {
                if (id != fridgeBrand.Id)
                {
                    return RedirectToAction("NotFoundPage", "Error");
                }
                if (ModelState.IsValid)
                {
                    if (_context.FridgeBrands.Any(m => m.Name == fridgeBrand.Name))
                    {
                        if (fridgeBrand.Name != editValue)
                        {
                            ModelState.AddModelError("Name", "The Name is Already Exist");
                            fridgeBrand.Name = editValue;
                            return View(fridgeBrand);
                        }
                    }

                    _context.Update(fridgeBrand);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }

                return View(fridgeBrand);
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }
        }


        // GET: Targets/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fridgeBrand = await _context.FridgeBrands.FirstOrDefaultAsync(m => m.Id == id);
            if (fridgeBrand == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            return View(fridgeBrand);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var fridgeBrand = await _context.FridgeBrands.FindAsync(id);
                _context.FridgeBrands.Remove(fridgeBrand);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (DbUpdateException)
            {

                string errorMsg = "FridgeBrand cannot be deleted. Because It is used in other Place (Ex. Fridge ). If you really want to Delete, Please remove those first which use this FridgeBrand and Try Again";
                return RedirectToAction("Delete", "Error", new { errorMsg = errorMsg });
            }

        }
    }
}