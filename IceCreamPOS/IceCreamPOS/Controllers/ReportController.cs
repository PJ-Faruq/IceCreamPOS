﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IceCreamPOS.Models;
using IceCreamPOS.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace IceCreamPOS.Controllers
{
    public class ReportController : Controller
    {

        private readonly IceCreamDbContext _context;

        public ReportController(IceCreamDbContext context)
        {
            _context = context;
        }

        public IActionResult BalanceSheet()
        {

            ViewBag.EnterpriseList = _context.Enterprises.ToList();
            return View();
        }

        [HttpPost]
        public IActionResult BalanceSheet(BalanceSheetSearchVm searchVm)
        {
            try
            {
                ViewBag.EnterpriseList = _context.Enterprises.ToList();
                ViewBag.Enterprise = _context.Enterprises.Find(searchVm.EnterpriseId).Name;
                ViewBag.DateRange = searchVm.FromDate.ToString("dd MMMM yyyy") + " -- " + searchVm.ToDate.ToString("dd MMMM yyyy");

                int numberOfDays = (searchVm.ToDate - searchVm.FromDate).Days;

                var purchaseList = _context.Purchase.Where(m => m.PurchaseDate >= searchVm.FromDate && m.PurchaseDate <= searchVm.ToDate && m.EnterpriseId == searchVm.EnterpriseId).ToList();
                var depositeList = _context.Deposites.Where(m => m.Date >= searchVm.FromDate && m.Date <= searchVm.ToDate && m.EnterpriseId == searchVm.EnterpriseId).ToList();
                List<BalanceSheetVM> listOfBalance = new List<BalanceSheetVM>();
                for (int i = 0; i <= numberOfDays; i++)
                {
                    List<Purchase> listOfPurchase = purchaseList.Where(m => m.PurchaseDate == searchVm.FromDate.AddDays(i)).ToList();
                    foreach (var purchase in listOfPurchase)
                    {
                        BalanceSheetVM balance = new BalanceSheetVM();

                        balance.Date = purchase.PurchaseDate;
                        balance.Invoice = purchase.TotalAmount;
                        balance.Particular = "Order";
                        balance.VoucherNo = purchase.PurchaseNumber;
                        balance.Deposite = 0;

                        listOfBalance.Add(balance);
                    }

                    List<Deposite> listOfDeposite = depositeList.Where(m => m.Date == searchVm.FromDate.AddDays(i)).ToList();
                    foreach (var deposite in listOfDeposite)
                    {
                        BalanceSheetVM balance = new BalanceSheetVM();
                        balance.Date = deposite.Date;
                        balance.Particular = deposite.Particular;
                        balance.VoucherNo = "";
                        balance.Invoice = 0;
                        balance.Deposite = deposite.Amount;

                        listOfBalance.Add(balance);
                    }


                }

                List<BalanceSheetVM> listOfFinalBalance = new List<BalanceSheetVM>();

                decimal totalBalance = 0;

                var listOfSaleBeforeFromDate = _context.Sales.Where(m => m.SalesDate < searchVm.FromDate).ToList();
                var listOfDepositeBeforeFromDate = _context.Deposites.Where(m => m.Date < searchVm.FromDate).ToList();

                decimal totalSaleAmount = listOfSaleBeforeFromDate.Sum(m => m.TotalAmount);
                decimal totalDepositeAmount = listOfDepositeBeforeFromDate.Sum(m => m.Amount);
                totalBalance = totalDepositeAmount - totalSaleAmount;

                if (listOfFinalBalance.Count > 0)
                {
                    totalBalance = listOfFinalBalance[0].Balance;
                }

                foreach (var item in listOfBalance)
                {
                    BalanceSheetVM balance = new BalanceSheetVM();
                    balance.Date = item.Date;
                    balance.Particular = item.Particular;
                    balance.VoucherNo = item.VoucherNo;
                    balance.Invoice = item.Invoice;
                    balance.Deposite = item.Deposite;

                    totalBalance += item.Deposite;
                    totalBalance -= item.Invoice;

                    balance.Balance = totalBalance;

                    if (item.Invoice != 0)
                    {
                        balance.Profit = (item.Invoice * 8) / 100;    // 8% of Invoice
                    }

                    listOfFinalBalance.Add(balance);
                }

                return View(listOfFinalBalance);
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }

            
        }

        //public JsonResult BalanceSheet(BalanceSheetSearchVm searchVm)
        //{
        //    int numberOfDays = (searchVm.ToDate - searchVm.FromDate).Days;

        //    var salesList = _context.Sales.Where(m => m.SalesDate >= searchVm.FromDate && m.SalesDate <= searchVm.ToDate).ToList();
        //    var depositeList = _context.Deposites.Where(m => m.Date >= searchVm.FromDate && m.Date <= searchVm.ToDate).ToList();
        //    List<BalanceSheetVM> listOfBalance = new List<BalanceSheetVM>();
        //    for (int i = 0; i < numberOfDays; i++)
        //    {
        //        List<Sale> listOfSales = salesList.Where(m => m.SalesDate == searchVm.FromDate.AddDays(i)).ToList();
        //        foreach (var sale in listOfSales)
        //        {
        //            BalanceSheetVM balance = new BalanceSheetVM();

        //            balance.Date = sale.SalesDate;
        //            balance.Invoice = sale.TotalAmount;
        //            balance.Particular = "Order";
        //            balance.VoucherNo = sale.SaleNumber;
        //            balance.Deposite = 0;

        //            listOfBalance.Add(balance);
        //        }

        //        List<Deposite> listOfDeposite = depositeList.Where(m => m.Date == searchVm.FromDate.AddDays(i)).ToList();
        //        foreach (var deposite in listOfDeposite)
        //        {
        //            BalanceSheetVM balance = new BalanceSheetVM();
        //            balance.Date = deposite.Date;
        //            balance.Particular = deposite.Particular;
        //            balance.VoucherNo = "";
        //            balance.Invoice = 0;
        //            balance.Deposite = deposite.Amount;

        //            listOfBalance.Add(balance);
        //        }


        //    }

        //    List<BalanceSheetVM> listOfFinalBalance = new List<BalanceSheetVM>();

        //    decimal totalBalance = 0;

        //    var listOfSaleBeforeFromDate = _context.Sales.Where(m => m.SalesDate < searchVm.FromDate).ToList();
        //    var listOfDepositeBeforeFromDate = _context.Deposites.Where(m => m.Date < searchVm.FromDate).ToList();

        //    decimal totalSaleAmount = listOfSaleBeforeFromDate.Sum(m => m.TotalAmount);
        //    decimal totalDepositeAmount = listOfDepositeBeforeFromDate.Sum(m => m.Amount);
        //    totalBalance = totalDepositeAmount - totalSaleAmount;

        //    if (listOfFinalBalance.Count > 0)
        //    {
        //        totalBalance = listOfFinalBalance[0].Balance;
        //    }

        //    foreach (var item in listOfBalance)
        //    {
        //        BalanceSheetVM balance = new BalanceSheetVM();
        //        balance.Date = item.Date;
        //        balance.Particular = item.Particular;
        //        balance.VoucherNo = item.VoucherNo;
        //        balance.Invoice = item.Invoice;
        //        balance.Deposite = item.Deposite;

        //        totalBalance += item.Deposite;
        //        totalBalance -= item.Invoice;

        //        balance.Balance = totalBalance;

        //        if (item.Invoice != 0)
        //        {
        //            balance.Profit = (item.Invoice * 7) / 100;    // 7% of Invoice
        //        }

        //        listOfFinalBalance.Add(balance);
        //    }

        //    return Json(listOfFinalBalance);
        //}
        public IActionResult TargetVsAchievement()
        {
            return View();
        }

        public IActionResult Stock()
        {

            ViewBag.FoodTypeList = new SelectList(_context.FoodTypes, "Id", "Name");
            ViewBag.EnterpriseList = new SelectList(_context.Enterprises, "Id", "Name");
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Stock(StockSearchVm searchVm)
        {
            try
            {
                var listOfStock = _context.Stocks.AsQueryable();
                if (searchVm.FoodTypeId != 0)
                {
                    listOfStock = listOfStock.Where(m => m.Item.FoodTypeId == searchVm.FoodTypeId).AsQueryable();
                }
                if (searchVm.EnterpriseId != 0)
                {
                    listOfStock = listOfStock.Where(m => m.EnterpriseId == searchVm.EnterpriseId).AsQueryable();
                }

                var stocks =await listOfStock.Include(x => x.Item).ThenInclude(y => y.FoodType).ToListAsync();

                List<StockVm> stockVms = new List<StockVm>();
                foreach (var stock in stocks)
                {
                    StockVm stockVm = new StockVm();
                    stockVm.ProductName = stock.Item.Name;
                    stockVm.ProductType = stock.Item.FoodType.Name;
                    stockVm.StockQuantity = stock.StockQuantity;
                    stockVm.Price = stock.Item.PurchasePrice;
                    stockVm.TotalPrice = stock.Item.PurchasePrice * stock.StockQuantity;
                    

                    stockVms.Add(stockVm);

                }

                ViewBag.FoodTypeList = new SelectList(_context.FoodTypes, "Id", "Name", searchVm.FoodTypeId);
                ViewBag.EnterpriseList = new SelectList(_context.Enterprises, "Id", "Name", searchVm.EnterpriseId);
                return View(stockVms);
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }


        }


        public IActionResult CustomerProfile()
        {
            ViewBag.EnterpriseList = new SelectList(_context.Enterprises, "Id", "Name");
            ViewBag.OutletList = new SelectList(_context.Outlets, "Id", "Name");
            ViewBag.AmountType = new SelectList(Common.GetAmountType(), "Name", "Name");
            //ViewBag.FromDate = "";
            //ViewBag.ToDate = "";
            //ViewBag.VoucherNumber = "";

            //List<Sale> listOfSale =await _context.Sales
            //                       .Include(x=>x.Enterprise)
            //                       .Include(y=>y.Outlet)
            //                       .ToListAsync();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CustomerProfile(CustomerProfileSearchVm searchVM)
        {
            try
            {

                ViewBag.EnterpriseList = new SelectList(_context.Enterprises, "Id", "Name", searchVM.EnterpriseId);
                ViewBag.OutletList = new SelectList(_context.Outlets, "Id", "Name", searchVM.OutletId);
                ViewBag.AmountType = new SelectList(Common.GetAmountType(), "Name", "Name", searchVM.AmountType);
                

                var outletList = _context.Outlets.ToList();
                var salesList = _context.Sales.Where(m => m.SalesDate.Month == searchVM.Month.Month && m.SalesDate.Year == searchVM.Month.Year && m.EnterpriseId == searchVM.EnterpriseId).ToList();
                var collectionList = _context.Collection.Where(m => m.Date.Month == searchVM.Month.Month && m.Date.Year == searchVM.Month.Year && m.EnterpriseId == searchVM.EnterpriseId).ToList();

                List<CustomerProfileVm> listOfCustomerProfile = new List<CustomerProfileVm>();

                foreach (var item in outletList)
                {
                    CustomerProfileVm profile = new CustomerProfileVm();

                    profile.EnterpriseId = searchVM.EnterpriseId;
                    profile.OutletName = item.Name;
                    profile.OutletId = item.Id;
                    profile.Month = searchVM.Month;
                    profile.TotalCollection = collectionList.Where(m => m.OutletId == item.Id).Sum(c => c.CollectionAmount);
                    profile.TotalAmount = salesList.Where(m => m.OutletId == item.Id).Sum(c => c.TotalAmount);
                    profile.Due = profile.TotalAmount - profile.TotalCollection;
                    //profile.TotalDue = _context.Sales.Where(m => m.OutletId == item.Id && m.EnterpriseId == searchVM.EnterpriseId).Sum(c => c.TotalAmount) - _context.Collection.Where(m => m.OutletId == item.Id && m.EnterpriseId == searchVM.EnterpriseId).Sum(c => c.CollectionAmount);

                    listOfCustomerProfile.Add(profile);
                }

                if (searchVM.OutletId != null)
                {
                    listOfCustomerProfile = listOfCustomerProfile.Where(m => m.OutletId == searchVM.OutletId).ToList();
                }

                if (searchVM.AmountType == "Due")
                {
                    listOfCustomerProfile = listOfCustomerProfile.Where(m => m.Due > 0).ToList();
                }

                if (searchVM.AmountType == "Full Paid")
                {
                    listOfCustomerProfile = listOfCustomerProfile.Where(m => m.Due <= 0).ToList();
                }

                var removeList = listOfCustomerProfile.Where(m => m.TotalAmount == 0 && m.TotalCollection == 0);

                return View(listOfCustomerProfile.Except(removeList));



            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }


        }


        public async Task<IActionResult> CollectionHistory(string val)
        {
            try
            {
                string[] value = val.Split('$');

                DateTime date = Convert.ToDateTime(value[0]);
                int enterpriseId = Convert.ToInt32(value[1]);
                int outletId = Convert.ToInt32(value[2]);

                var collectionList = await _context.Collection.Where(m => m.Date.Month == date.Month && m.Date.Year == date.Year && m.EnterpriseId == enterpriseId && m.OutletId == outletId)
                                    .Include(x => x.Enterprise)
                                    .Include(y => y.Outlet)
                                    .ToListAsync();

                return View(collectionList);
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }

            
        }


        [HttpGet]
        public IActionResult CustomerDueList()
        {
            ViewBag.EnterpriseList = new SelectList(_context.Enterprises, "Id", "Name");
            ViewBag.OutletList = new SelectList(_context.Outlets, "Id", "Name");
            ViewBag.AmountType = new SelectList(Common.GetAmountType(), "Name", "Name");
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CustomerDueList(CustomerProfileSearchVm searchVM)
        {
            try
            {

                ViewBag.EnterpriseList = new SelectList(_context.Enterprises, "Id", "Name", searchVM.EnterpriseId);
                ViewBag.OutletList = new SelectList(_context.Outlets, "Id", "Name", searchVM.OutletId);


                var outletList = _context.Outlets.ToList();

                List<CustomerProfileVm> listOfCustomerProfile = new List<CustomerProfileVm>();

                foreach (var item in outletList)
                {
                    CustomerProfileVm profile = new CustomerProfileVm();

                    profile.OutletName = item.Name;
                    profile.OutletId = item.Id;
                    profile.TotalDue = _context.Sales.Where(m => m.OutletId == item.Id && m.EnterpriseId == searchVM.EnterpriseId).Sum(c => c.TotalAmount) - _context.Collection.Where(m => m.OutletId == item.Id && m.EnterpriseId == searchVM.EnterpriseId).Sum(c => c.CollectionAmount);

                    listOfCustomerProfile.Add(profile);
                }

                if (searchVM.OutletId != null)
                {
                    listOfCustomerProfile = listOfCustomerProfile.Where(m => m.OutletId == searchVM.OutletId).ToList();
                }

                //var removeList = listOfCustomerProfile.Where(m => m.TotalAmount == 0 && m.TotalCollection == 0);

                return View(listOfCustomerProfile);



            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }


        }
    }
}