﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IceCreamPOS.Models;
using IceCreamPOS.ViewModels;

namespace IceCreamPOS.Controllers
{
    public class CollectionsController : Controller
    {
        private readonly IceCreamDbContext _context;

        public CollectionsController(IceCreamDbContext context)
        {
            _context = context;
        }

        // GET: Collections
        public async Task<IActionResult> Index()
        {
            var iceCreamDbContext = _context.Collection.Include(c => c.Outlet);
            return View(await iceCreamDbContext.ToListAsync());
        }

        // GET: Collections/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var collection = await _context.Collection
                .Include(c => c.Outlet)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (collection == null)
            {
                return NotFound();
            }

            return View(collection);
        }

        // GET: Collections/Create
        public IActionResult Create()
        {
            ViewBag.successMsg = "No";

            ViewData["EnterpriseId"] = new SelectList(_context.Enterprises, "Id", "Name");
            ViewData["OutletId"] = new SelectList(_context.Outlets, "Id", "Name");
            return View();
        }

        // POST: Collections/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create( CollectionVm collectionVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    //Create Collection history
                    Collection collection = new Collection();
                    collection.OutletId = collectionVm.OutletId;
                    collection.Particular = collectionVm.Particular;
                    collection.CollectionAmount = collectionVm.NewCollection;
                    collection.Date = collectionVm.Date;
                    collection.EnterpriseId = collectionVm.EnterpriseId;
                    await _context.Collection.AddAsync(collection);
                    bool status= _context.SaveChanges()>0;

                    if (status)
                    {
                        ModelState.Clear();
                        ViewBag.successMsg = "New Collection Successfully Added";

                        ViewData["EnterpriseId"] = new SelectList(_context.Enterprises, "Id", "Name");
                        ViewData["OutletId"] = new SelectList(_context.Outlets, "Id", "Name");
                        return View();
                    }
                }
                ViewBag.successMsg = "No";
                ViewData["EnterpriseId"] = new SelectList(_context.Enterprises, "Id", "Name");
                ViewData["OutletId"] = new SelectList(_context.Outlets, "Id", "Name", collectionVm.OutletId);
                return View(collectionVm);
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }
            
        }

        // GET: Collections/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var collection = await _context.Collection.FindAsync(id);
            if (collection == null)
            {
                return NotFound();
            }
            ViewData["OutletId"] = new SelectList(_context.Outlets, "Id", "Name", collection.OutletId);
            return View(collection);
        }

        // POST: Collections/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,OutletId,TotalAmount,TotalCollection,TotalDue")] Collection collection)
        {
            if (id != collection.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(collection);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CollectionExists(collection.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["OutletId"] = new SelectList(_context.Outlets, "Id", "Name", collection.OutletId);
            return View(collection);
        }

        // GET: Collections/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var collection = await _context.Collection
                .Include(c => c.Outlet)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (collection == null)
            {
                return NotFound();
            }

            return View(collection);
        }

        // POST: Collections/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var collection = await _context.Collection.FindAsync(id);
            _context.Collection.Remove(collection);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CollectionExists(int id)
        {
            return _context.Collection.Any(e => e.Id == id);
        }

        public JsonResult GetCollectionByOutletId(int outletId,int enterpriseId)
        {
            decimal totalSale = _context.Sales.Where(c => c.OutletId == outletId && c.EnterpriseId==enterpriseId).Sum(m => m.TotalAmount);
            decimal totalCollection = _context.Collection.Where(c => c.OutletId == outletId && c.EnterpriseId == enterpriseId).Sum(m => m.CollectionAmount);
            decimal totalDue = totalSale - totalCollection;
            return Json(totalDue);
        }
    }
}
