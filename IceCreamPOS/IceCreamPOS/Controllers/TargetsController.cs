﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IceCreamPOS.Models;
using IceCreamPOS.ViewModels;

namespace IceCreamPOS.Controllers
{
    public class TargetsController : Controller
    {
        private readonly IceCreamDbContext _context;

        public TargetsController(IceCreamDbContext context)
        {
            _context = context;
        }

        // GET: Target
        public async Task<IActionResult> Index()
        {
            return View(await _context.Target
                .Include(x => x.FoodType)
                .Include(y=>y.Enterprise)
                .ToListAsync());
        }

        // GET: Target/Create
        public IActionResult Create()
        {
            ViewBag.EnterpriseId = new SelectList(_context.Enterprises, "Id", "Name");
            ViewBag.FoodTypeId = new SelectList(_context.FoodTypes, "Id", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Target target)
        {
            try
            {
                ViewBag.EnterpriseId = new SelectList(_context.Enterprises, "Id", "Name", target.EnterpriseId);
                ViewBag.FoodTypeId = new SelectList(_context.FoodTypes, "Id", "Name", target.FoodTypeId);

                if (ModelState.IsValid)
                {


                    if (_context.Target.Any(m => m.Month == target.Month && m.EnterpriseId == target.EnterpriseId && m.FoodTypeId == target.FoodTypeId))
                    {
                        ModelState.AddModelError("Month", "The Target of this Month is Already Added");
                        return View(target);
                    }

                    _context.Add(target);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }

                return View(target);
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }

        }

        // GET: Target/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            var target = await _context.Target.FindAsync(id);
            if (target == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            ViewBag.EnterpriseId = new SelectList(_context.Enterprises, "Id", "Name", target.EnterpriseId);
            ViewBag.FoodTypeId = new SelectList(_context.FoodTypes, "Id", "Name", target.FoodTypeId);
            return View(target);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Target target)
        {
            try
            {
                if (id != target.Id)
                {
                    return RedirectToAction("NotFoundPage", "Error");
                }

                ViewBag.EnterpriseId = new SelectList(_context.Enterprises, "Id", "Name", target.EnterpriseId);
                ViewBag.FoodTypeId = new SelectList(_context.FoodTypes, "Id", "Name", target.FoodTypeId);

                if (ModelState.IsValid)
                {

                    _context.Update(target);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }

                return View(target);
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }
        }


        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            var target = await _context.Target
                .Include(t => t.FoodType)
                .Include(s=>s.Enterprise)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (target == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            return View(target);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var target = await _context.Target.FindAsync(id);
                _context.Target.Remove(target);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (DbUpdateException)
            {

                string errorMsg = "Target cannot be deleted. Because It is used in other Place (Ex. Target vs Achievement). If you really want to Delete, Please remove those first which use this Target and Try Again";
                return RedirectToAction("Delete", "Error", new { errorMsg = errorMsg });
            }

        }

        private bool TargetExists(int id)
        {
            return _context.Target.Any(e => e.Id == id);
        }

        public JsonResult GetTargetResult(DateTime date)
        {
            int month = date.Month;
            int year = date.Year;

            // Get Target and Achievement for L.R Enterprise for Ice Cream
            var listForLRIce = _context.SalesDetails.Where(m => m.Sale.SalesDate.Month == month && m.Sale.SalesDate.Year == year && m.Sale.Enterprise.Name == "L.R Enterprise" && m.Item.FoodType.Name == "Ice Cream").ToList();
            var targetForLRIce = _context.Target.FirstOrDefault(m => m.Month.Month == month && m.Month.Year == year && m.Enterprise.Name == "L.R Enterprise" && m.FoodType.Name == "Ice Cream");
            decimal totalSaleForLRIce =listForLRIce.Sum(m => m.TotalPrice);

            decimal totalTargetForLRIce = 0;
            if (targetForLRIce != null)
            {
                totalTargetForLRIce = targetForLRIce.TargetAmount;
            }

            decimal percentageForLRIce = 0;
            if (totalTargetForLRIce != 0)
            {
                percentageForLRIce = Math.Round((totalSaleForLRIce * 100) / totalTargetForLRIce, 2);
            }
            
           

            // Get Target and Achievement for L.R Enterprise for Frozen Food
            var listForLRFrozen = _context.SalesDetails.Where(m => m.Sale.SalesDate.Month == month && m.Sale.SalesDate.Year == year && m.Sale.Enterprise.Name == "L.R Enterprise" && m.Item.FoodType.Name == "Frozen Food").ToList();
            var targetForLRFrozen = _context.Target.FirstOrDefault(m => m.Month.Month == month && m.Month.Year == year && m.Enterprise.Name == "L.R Enterprise" && m.FoodType.Name == "Frozen Food");
            decimal totalSaleForLRFrozen = listForLRFrozen.Sum(m => m.TotalPrice);

            decimal totalTargetForLRFrozen = 0;
            if (targetForLRFrozen != null)
            {
                totalTargetForLRFrozen = targetForLRFrozen.TargetAmount;
            }

            decimal percentageForLRFrozen = 0;

            if (totalTargetForLRFrozen != 0)
            {
                percentageForLRFrozen = Math.Round((totalSaleForLRFrozen * 100) / totalTargetForLRFrozen, 2);
            }

            // Get Target and Achievement for Toufiq Enterprise for Ice Cream
            var listForToufiq = _context.SalesDetails.Where(m => m.Sale.SalesDate.Month == month && m.Sale.SalesDate.Year == year && m.Sale.Enterprise.Name == "Toufiq Enterprise" && m.Item.FoodType.Name == "Ice Cream").ToList();
            var targetForToufiq = _context.Target.FirstOrDefault(m => m.Month.Month == month && m.Month.Year == year && m.Enterprise.Name == "Toufiq Enterprise" && m.FoodType.Name == "Ice Cream");
            decimal totalSaleForToufiq = listForToufiq.Sum(m => m.TotalPrice);

            decimal totalTargetForToufiq = 0;
            if (targetForToufiq != null)
            {
                totalTargetForToufiq = targetForToufiq.TargetAmount;
            }

            decimal percentageForToufiq = 0;
            if (totalTargetForToufiq != 0)
            {
                percentageForToufiq = Math.Round((totalSaleForToufiq * 100) / totalTargetForToufiq, 2);
            }

            List<TargetVm> targetVms = new List<TargetVm>()
            {
                new TargetVm()
                {
                    Achievement=totalSaleForLRIce,
                    Target=totalTargetForLRIce,
                    Percentage=percentageForLRIce
                },
                new TargetVm()
                {
                    Achievement=totalSaleForLRFrozen,
                    Target=totalTargetForLRFrozen,
                    Percentage=percentageForLRFrozen
                },
                new TargetVm()
                {
                    Achievement=totalSaleForToufiq,
                    Target=totalTargetForToufiq,
                    Percentage=percentageForToufiq
                }
            };

            return Json(targetVms);
        }
    }
}
