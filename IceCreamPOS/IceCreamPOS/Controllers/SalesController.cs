﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IceCreamPOS.Models;
using IceCreamPOS.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace IceCreamPOS.Controllers
{
    public class SalesController : Controller
    {

        private readonly IceCreamDbContext _context;

        public SalesController(IceCreamDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            return View(await _context.Sales.Where(m => m.IsDeleted == false)
                .Include(c=>c.Enterprise)
                .Include(y=>y.Outlet)
                .ToListAsync());
        }

        public async Task<IActionResult> Create()
        {
            ViewBag.SaleNumber = GenerateCode();
            ViewBag.EnterpriseList = await _context.Enterprises.ToListAsync();
            ViewBag.FoodTypeList = await _context.FoodTypes.ToListAsync();
            ViewBag.OutletList = await _context.Outlets.ToListAsync();

            return View();
        }


        [HttpPost]
        public JsonResult Create(Sale sale)
        {
            try
            {
                bool status = false;
                if (ModelState.IsValid)
                {
                    _context.Sales.Add(sale);
                    status = _context.SaveChanges() > 0;

                    if (status)
                    {

                        //Update Stock
                        foreach (var item in sale.SalesDetails)
                        {

                            var stockItem = _context.Stocks.FirstOrDefault(m => m.ItemId == item.ItemId && m.EnterpriseId==sale.EnterpriseId);

                            if (stockItem != null)
                            {
                                stockItem.StockQuantity = stockItem.StockQuantity - item.Quantity;
                                _context.Stocks.Attach(stockItem);
                                _context.Entry(stockItem).State = EntityState.Modified;
                                _context.SaveChanges();
                            }
                        }

                        //Update Collection 
                        if (sale.TotalCollection > 0)
                        {
                            UpdateCollection(sale);
                        }
                        

                        var successResult = new { code = GenerateCode(), status = "Yes" };
                        return Json(successResult);
                    }



                }

                var errorResult = new { code = GenerateCode(), status = "No" };
                return Json(errorResult);
            }
            catch (Exception )
            {

                var errorResult = new { code = GenerateCode(), status = "No" };
                return Json(errorResult);
            }

        }

        private void UpdateCollection(Sale sale)
        {   
            Collection collection = new Collection();
            collection.OutletId = sale.OutletId;
            collection.Particular = "Sale";
            collection.CollectionAmount = sale.TotalCollection;
            collection.Date = sale.SalesDate;
            collection.EnterpriseId = sale.EnterpriseId;
            _context.Collection.Add(collection);
            _context.SaveChanges();
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            var item = await _context.Sales
                .Include(c => c.Enterprise)
                .Include(y => y.Outlet)
                .Include(z=>z.SalesDetails).ThenInclude(x=>x.Item)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (item == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            return View(item);
        }

        public JsonResult GetItemById(int itemId,int enterpriseId)
        {

            Item item = _context.Items.Find(itemId);

            int stockQuantity = 0;

            var stock = _context.Stocks.FirstOrDefault(m => m.ItemId == itemId && m.EnterpriseId==enterpriseId);

            if (stock != null)
            {
                stockQuantity = stock.StockQuantity;
            }

            var newItem = new { piecePerCarton = item.PiecePerCarton, salesPrice = item.SalesPrice, stockQuantity = stockQuantity };
            return Json(newItem);
        }

        private string GenerateCode()
        {
            string code = "";
            if (_context.Sales.Count() <= 0)
            {
                code = "1";
            }
            else
            {
                code = (_context.Sales.Max(m => m.Id) + 1).ToString();
            }

            string generatedCode = code.ToString();

            if (generatedCode.Length == 1)
            {
                generatedCode = "SN-" + "0" + "0" + code;
            }
            if (code.Length == 2)
            {
                generatedCode = "SN-" + "0" + code;
            }
            if (code.Length >= 3)
            {
                generatedCode = "SN-" + code;
            }

            return generatedCode;

        }
    }
}