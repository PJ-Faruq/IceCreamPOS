﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IceCreamPOS.Models;

namespace IceCreamPOS.Controllers
{
    public class ItemsController : Controller
    {
        private readonly IceCreamDbContext _context;

        public ItemsController(IceCreamDbContext context)
        {
            _context = context;
        }

        // GET: Items
        public async Task<IActionResult> Index()
        {
            return View(await _context.Items.Include(x=>x.FoodType).ToListAsync());
        }

        // GET: Items/Create
        public IActionResult Create()
        {
            ViewBag.FoodTypeList = _context.FoodTypes.ToList();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Item item)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if(_context.Items.Any(m=>m.Name==item.Name && m.FoodTypeId == item.FoodTypeId ))
                    {
                        ModelState.AddModelError("Name", "The Name is Already Exist");
                        
                    }
                    else
                    {
                        _context.Add(item);
                        await _context.SaveChangesAsync();
                        return RedirectToAction(nameof(Index));
                    }

                }
                ViewBag.FoodTypeList = _context.FoodTypes.ToList();
                return View(item);
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown","Error");
            }

        }

        // GET: Items/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            var item = await _context.Items.FindAsync(id);
            if (item == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }
            ViewBag.FoodTypeId = new SelectList(_context.FoodTypes, "Id", "Name", item.FoodTypeId);
            return View(item);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Item item, string editValue)
        {
            try
            {
                if (id != item.Id)
                {
                    return RedirectToAction("NotFoundPage", "Error");
                }

                ViewBag.FoodTypeId = new SelectList(_context.FoodTypes, "Id", "Name", item.FoodTypeId);
                if (ModelState.IsValid)
                {
                    if (_context.Items.Any(m => m.Name == item.Name && m.FoodTypeId == item.FoodTypeId))
                    {
                        if (item.Name != editValue)
                        {
                            ModelState.AddModelError("Name", "The Name is Already Exist");
                            item.Name = editValue;
                            return View(item);
                        }
                    }

                    _context.Update(item);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }

                return View(item);
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }  
        }


        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            var item = await _context.Items
                .Include(t => t.FoodType)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (item == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            return View(item);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var item = await _context.Items.FindAsync(id);
                _context.Items.Remove(item);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (DbUpdateException)
            {

                string errorMsg = "Item cannot be deleted. Because It is used in other Place (Ex. Purchase, Sale, Stock). If you really want to Delete, Please remove those first which use this Item and Try Again";
                return RedirectToAction("Delete", "Error", new { errorMsg = errorMsg });
            }

        }

        private bool ItemExists(int id)
        {
            return _context.Items.Any(e => e.Id == id);
        }

        public JsonResult GetItemById(int itemId)
        {
            Item item = _context.Items.Find(itemId);
            return Json(item);
        }


    }
}
