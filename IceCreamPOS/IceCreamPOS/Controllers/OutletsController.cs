﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IceCreamPOS.Models;
using IceCreamPOS.ViewModels;

namespace IceCreamPOS.Controllers
{
    public class OutletsController : Controller
    {
        private readonly IceCreamDbContext _context;

        public OutletsController(IceCreamDbContext context)
        {
            _context = context;
        }

        // GET: Outlets
        public IActionResult Index()
        {
            try
            {
                List<Outlet> outletList = _context.Outlets
               .Include(x => x.Fridge)
               .Include(y => y.OutletType)
               .ToList();

                List<OutletVm> outletVms = new List<OutletVm>();
                foreach (var outlet in outletList)
                {
                    OutletVm outletVm = new OutletVm();
                    outletVm.Id = outlet.Id;
                    outletVm.Name = outlet.Name;
                    outletVm.Address = outlet.Address != null ? outlet.Address : "-";
                    string newDate = "";
                    if (outlet.FridgeIssueDate != null)
                    {
                        string date = outlet.FridgeIssueDate.ToString();
                        string[] dt = date.Split(' ');
                        newDate = Convert.ToDateTime(dt[0]).ToString("dd/MM/yyyy");
                    }

                    outletVm.FridgeIssueDate = outlet.FridgeIssueDate == null ? "-" : newDate;
                    outletVm.FridgeName = outlet.FridgeId != null ? outlet.Fridge.Name : "-";
                    outletVm.OutletType = outlet.OutletType.Name;
                    outletVm.PhoneNumber = outlet.PhoneNumber != "" ? outlet.PhoneNumber : "-";

                    outletVms.Add(outletVm);

                }
                return View(outletVms);
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }
           
        }

        // GET: Outlets/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var outlet = await _context.Outlets
                .FirstOrDefaultAsync(m => m.Id == id);
            if (outlet == null)
            {
                return NotFound();
            }

            return View(outlet);
        }

        // GET: Outlets/Create
        public IActionResult Create()
        {
            ViewBag.OutletTypeList = _context.OutletTypes.ToList();
            ViewBag.FridgeList = _context.Fridges.ToList();
            return View();
        }

        // POST: Outlets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create( Outlet outlet)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _context.Add(outlet);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                ViewBag.OutletTypeList = _context.OutletTypes.ToList();
                ViewBag.FridgeList = _context.Fridges.ToList();
                return View(outlet);
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }

        }

        // GET: Outlets/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            var outlet = await _context.Outlets.FindAsync(id);
            if (outlet == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }
            ViewBag.OutletTypeList = new SelectList(_context.OutletTypes, "Id", "Name", outlet.OutletTypeId);
            ViewBag.FridgeList = new SelectList(_context.Fridges, "Id", "Name", outlet.FridgeId);
            return View(outlet);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Outlet outlet, string editValue)
        {
            try
            {
                if (id != outlet.Id)
                {
                    return RedirectToAction("NotFoundPage", "Error");
                }

                ViewBag.OutletTypeList = new SelectList(_context.OutletTypes, "Id", "Name", outlet.OutletTypeId);
                ViewBag.FridgeList = new SelectList(_context.Fridges, "Id", "Name", outlet.FridgeId);
                if (ModelState.IsValid)
                {
                    if (_context.Outlets.Any(m => m.Name == outlet.Name))
                    {
                        if (outlet.Name != editValue)
                        {
                            ModelState.AddModelError("Name", "The Name is Already Exist");
                            outlet.Name = editValue;
                            return View(outlet);
                        }
                    }

                    _context.Update(outlet);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }

                return View(outlet);
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }
        }


        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            var item = await _context.Outlets
                .FirstOrDefaultAsync(m => m.Id == id);
            if (item == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            return View(item);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var item = await _context.Outlets.FindAsync(id);
                _context.Outlets.Remove(item);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (DbUpdateException)
            {

                string errorMsg = "Item cannot be deleted. Because It is used in other Place (Ex. Purchase, Sale, Stock). If you really want to Delete, Please remove those first which use this Item and Try Again";
                return RedirectToAction("Delete", "Error", new { errorMsg = errorMsg });
            }

        }

        private bool OutletExists(int id)
        {
            return _context.Outlets.Any(e => e.Id == id);
        }
    }
}
