﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IceCreamPOS.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IceCreamPOS.Controllers
{
    public class PurchasesController : Controller
    {
        private readonly IceCreamDbContext _context;

        public PurchasesController(IceCreamDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            var list = await _context.Purchase.Include(m => m.Enterprise).ToListAsync();
            return View(list);
        }

        public async Task<IActionResult> Create()
        {
            ViewBag.PurchaseNumber = GenerateCode();
            ViewBag.EnterpriseList = await _context.Enterprises.ToListAsync();
            ViewBag.FoodTypeList =await _context.FoodTypes.ToListAsync();
            return View();
        }

        private string GenerateCode()
        {
            string code = "";
            if (_context.Purchase.Count() <= 0)
            {
                code = "1";
            }
            else
            {
                code = (_context.Purchase.Max(m => m.Id) + 1).ToString();
            }

            string generatedCode = code.ToString();

            if (generatedCode.Length == 1)
            {
                generatedCode = "PN-" + "0" + "0" + code;
            }
            if (code.Length == 2)
            {
                generatedCode = "PN-" + "0" + code;
            }
            if (code.Length >= 3)
            {
                generatedCode = "PN-" + code;
            }

            return generatedCode;

        }

        [HttpPost]
        public JsonResult Create(Purchase purchase)
        {
            try
            {
                bool status = false;
                if (ModelState.IsValid)
                {
                     _context.Purchase.Add(purchase);
                    status =  _context.SaveChanges() > 0;

                    if (status)
                    {
                        foreach (var item in purchase.PurchaseDetails)
                        {

                            var stockItem = _context.Stocks.FirstOrDefault(m => m.ItemId == item.ItemId && m.EnterpriseId == purchase.EnterpriseId);

                            if (stockItem != null)
                            {
                                stockItem.StockQuantity = stockItem.StockQuantity + item.Quantity;
                                _context.Stocks.Attach(stockItem);
                                _context.Entry(stockItem).State = EntityState.Modified;
                                 _context.SaveChanges();
                            }

                            else
                            {
                                if(stockItem == null)
                                {
                                    stockItem = new Stock();
                                }
                                stockItem.ItemId = item.ItemId;
                                stockItem.StockQuantity = item.Quantity;
                                stockItem.EnterpriseId = purchase.EnterpriseId;
                                 _context.Add(stockItem);
                                 _context.SaveChanges();
                            }
                        }

                        var successResult = new { code = GenerateCode(), status = "Yes" };
                        return Json(successResult);
                    }

                }


                var errorResult = new { code = GenerateCode(), status = "No"};
                return Json(errorResult);
            }
            catch (Exception )
            {

                var errorResult = new { code = GenerateCode(), status = "No" };
                return Json(errorResult);
            }

        }


        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            var item = await _context.Purchase
                .Include(c => c.Enterprise)
                .Include(z => z.PurchaseDetails).ThenInclude(x => x.Item)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (item == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            return View(item);
        }


        [HttpPost]
        public JsonResult GetItemListByFoodType(int foodTypeId)
        {
            var itemList = _context.Items.Where(m=>m.FoodTypeId==foodTypeId).ToList();
            return Json(itemList);
        }

        public JsonResult GetItemById(int itemId)
        {
            Item item = _context.Items.Find(itemId);
            return Json(item);
        }
    }
}