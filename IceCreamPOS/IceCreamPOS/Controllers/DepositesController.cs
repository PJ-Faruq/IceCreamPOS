﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IceCreamPOS.Models;

namespace IceCreamPOS.Controllers
{
    public class DepositesController : Controller
    {
        private readonly IceCreamDbContext _context;

        public DepositesController(IceCreamDbContext context)
        {
            _context = context;
        }


        // GET: Deposites
        public async Task<IActionResult> Index()
        {
            return View(await _context.Deposites.Include(m=>m.Enterprise).ToListAsync());
        }

        // GET: Deposites/Create
        public IActionResult Create()
        {
            ViewBag.EnterpriseList = new SelectList(_context.Enterprises, "Id", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Deposite deposite)
        {
            try
            {
                if (ModelState.IsValid)
                {
                        _context.Add(deposite);
                        await _context.SaveChangesAsync();
                        return RedirectToAction(nameof(Index));

                }

                ViewBag.EnterpriseList = new SelectList(_context.Enterprises, "Id", "Name");
                return View(deposite);
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }

        }

        // GET: Deposites/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            var deposite = await _context.Deposites.FindAsync(id);
            if (deposite == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }
            ViewBag.EnterpriseList = new SelectList(_context.Enterprises, "Id", "Name",deposite.EnterpriseId);
            return View(deposite);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Deposite deposite)
        {
            try
            {
                if (id != deposite.Id)
                {
                    return RedirectToAction("NotFoundPage", "Error");
                }

                if (ModelState.IsValid)
                {
                    _context.Update(deposite);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }

                ViewBag.EnterpriseList = new SelectList(_context.Enterprises, "Id", "Name",deposite.EnterpriseId);
                return View(deposite);
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }
        }


        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            var deposite = await _context.Deposites
                .FirstOrDefaultAsync(m => m.Id == id);
            if (deposite == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            return View(deposite);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var deposite = await _context.Deposites.FindAsync(id);
                _context.Deposites.Remove(deposite);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (DbUpdateException)
            {

                string errorMsg = "Deposite cannot be deleted. Because It is used in other Place (Ex. Target). If you really want to Delete, Please remove those first which use this Deposite and Try Again";
                return RedirectToAction("Delete", "Error", new { errorMsg = errorMsg });
            }

        }

    
    }
}
