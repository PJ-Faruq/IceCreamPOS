﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IceCreamPOS.Models;

namespace IceCreamPOS.Controllers
{
    public class PromotionalOffersController : Controller
    {
        private readonly IceCreamDbContext _context;

        public PromotionalOffersController(IceCreamDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            return View(await _context.PromotionalOffer.ToListAsync());
        }

        [HttpPost]
        public async Task<IActionResult> Index(DateTime? date)
        {
            if (date != null)
            {
                var list = _context.PromotionalOffer.Where(m => m.Date == date).ToList();
                return View(list);
            }
            return View(await _context.PromotionalOffer.ToListAsync());
        }

        // GET: PromotionalOffer/Create
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(PromotionalOffer promotion)
        {
            try
            {
                if (_context.PromotionalOffer.Any(m => m.Date == promotion.Date))
                {
                    ModelState.AddModelError("Date", "The value of this month is already Added");
                    return View(promotion);
                }

                if (promotion.DistributorAdditionalIncentive == null)
                {
                    promotion.DistributorAdditionalIncentive = 0;
                }

                if (promotion.DistributorIncentive == null)
                {
                    promotion.DistributorIncentive = 0;
                }

                if (promotion.DSRIncentive == null)
                {
                    promotion.DSRIncentive = 0;
                }

                if (promotion.RetailDiscount == null)
                {
                    promotion.RetailDiscount = 0;
                }

                if (ModelState.IsValid)
                {
                        _context.Add(promotion);
                        await _context.SaveChangesAsync();
                        return RedirectToAction(nameof(Index));
                }
                return View(promotion);
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }

        }

        // GET: PromotionalOffer/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            var promotionalOffer = await _context.PromotionalOffer.FindAsync(id);
            if (promotionalOffer == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }
            return View(promotionalOffer);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, PromotionalOffer promotionalOffer, string editValue)
        {
            try
            {
                if (id != promotionalOffer.Id)
                {
                    return RedirectToAction("NotFoundPage", "Error");
                }

                if (ModelState.IsValid)
                {
                    _context.Update(promotionalOffer);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }

                return View(promotionalOffer);
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }
        }


        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            var promotionalOffer = await _context.PromotionalOffer
                .FirstOrDefaultAsync(m => m.Id == id);
            if (promotionalOffer == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            return View(promotionalOffer);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var promotionalOffer = await _context.PromotionalOffer.FindAsync(id);
                _context.PromotionalOffer.Remove(promotionalOffer);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (DbUpdateException)
            {

                string errorMsg = "PromotionalOffer cannot be deleted. Because It is used in other Place (Ex. Purchase, Sale, Stock). If you really want to Delete, Please remove those first which use this PromotionalOffer and Try Again";
                return RedirectToAction("Delete", "Error", new { errorMsg = errorMsg });
            }

        }

        private bool PromotionalOfferExists(int id)
        {
            return _context.PromotionalOffer.Any(e => e.Id == id);
        }
    }
}
