﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IceCreamPOS.Models;
using IceCreamPOS.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace IceCreamPOS.Controllers
{
    public class BadGoodsReturnsController : Controller
    {

        private readonly IceCreamDbContext _context;

        public BadGoodsReturnsController(IceCreamDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Create()
        {
            ViewBag.ItemList = await _context.Items.ToArrayAsync();
            ViewBag.EnterpriseList = await _context.Enterprises.ToListAsync();
            return View();
        }

        [HttpPost]
        public JsonResult Create(BadGoodsReturn badGoodsReturn)
        {
            try
            {
                bool status = false;
                if (ModelState.IsValid)
                {
                    _context.BadGoodsReturn.Add(badGoodsReturn);
                    status = _context.SaveChanges() > 0;

                    if (status)
                    {
                        return Json(1);
                    }

                }

                return Json(0);
            }
            catch (Exception)
            {

                return Json(0);
            }

        }

        public async Task<IActionResult> Index()
        {
            ViewBag.EnterpriseList = new SelectList(_context.Enterprises, "Id", "Name");
            var badGoodsList =await _context.BadGoodsReturn.Include(m => m.Enterprise).Include(x=>x.BadGoodsReturnDetails).ToListAsync();
            return View(badGoodsList);
        }

        [HttpPost]
        public async Task<IActionResult> Index(BadGoodSearchVm searchVm)
        {
            try
            {
                var listOfBadGoods= _context.BadGoodsReturn.AsQueryable();

                if (searchVm.EnterpriseId != 0)
                {
                    listOfBadGoods = listOfBadGoods.Where(m => m.EnterpriseId == searchVm.EnterpriseId).AsQueryable();
                }

                if (searchVm.FromDate != null)
                {
                    listOfBadGoods = listOfBadGoods.Where(m => m.Date >=searchVm.FromDate).AsQueryable();
                }

                if (searchVm.ToDate != null)
                {
                    listOfBadGoods = listOfBadGoods.Where(m => m.Date <= searchVm.ToDate).AsQueryable();
                }

                var BadGoods = await listOfBadGoods.Include(m => m.Enterprise).Include(x => x.BadGoodsReturnDetails).ToListAsync();

                ViewBag.EnterpriseList = new SelectList(_context.Enterprises, "Id", "Name", searchVm.EnterpriseId);
                return View(BadGoods);
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }


        }


        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            var item = await _context.BadGoodsReturn
                .Include(c => c.Enterprise)
                .Include(z => z.BadGoodsReturnDetails).ThenInclude(x => x.Item)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (item == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            return View(item);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            var item = await _context.BadGoodsReturn
                .Include(t => t.Enterprise)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (item == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            return View(item);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var badGoodDetails = _context.BadGoodsReturnDetails.Where(m => m.BadGoodsReturnId == id).ToList();
                _context.BadGoodsReturnDetails.RemoveRange(badGoodDetails);

                var item = await _context.BadGoodsReturn.FindAsync(id);
                _context.BadGoodsReturn.Remove(item);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (DbUpdateException)
            {

                string errorMsg = "Item cannot be deleted. Because It is used in other Place (Ex. Purchase, Sale, Stock). If you really want to Delete, Please remove those first which use this Item and Try Again";
                return RedirectToAction("Delete", "Error", new { errorMsg = errorMsg });
            }

        }
    }
}