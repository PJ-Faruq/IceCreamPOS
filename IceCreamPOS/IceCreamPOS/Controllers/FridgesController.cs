﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IceCreamPOS.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace IceCreamPOS.Controllers
{
    public class FridgesController : Controller
    {
        private readonly IceCreamDbContext _context;

        public FridgesController(IceCreamDbContext context)
        {
            _context = context;
        }


        // GET: Fridges
        public async Task<IActionResult> Index()
        {
            return View(await _context.Fridges.Include(m=>m.FridgeBrand).ToListAsync());
        }

        // GET: Fridges/Create
        public IActionResult Create()
        {
            ViewBag.FridgeBrandList = _context.FridgeBrands.ToList();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Fridge fridge)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (_context.Fridges.Any(m => m.Name == fridge.Name && m.FridgeBrandId == fridge.FridgeBrandId))
                    {
                        ModelState.AddModelError("Name", "The Name is Already Exist");

                    }
                    else
                    {
                        _context.Add(fridge);
                        await _context.SaveChangesAsync();
                        return RedirectToAction(nameof(Index));
                    }

                }
                ViewBag.FridgeBrandList = _context.FridgeBrands.ToList();
                return View(fridge);
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }

        }

        // GET: Fridges/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            var fridge = await _context.Fridges.FindAsync(id);
            if (fridge == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }
            ViewBag.FridgeBrandList = new SelectList(_context.FridgeBrands, "Id", "Name", fridge.FridgeBrandId);
            return View(fridge);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Fridge fridge, string editValue)
        {
            try
            {
                if (id != fridge.Id)
                {
                    return RedirectToAction("NotFoundPage", "Error");
                }

                ViewBag.FridgeBrandList = new SelectList(_context.FridgeBrands, "Id", "Name", fridge.FridgeBrandId);
                if (ModelState.IsValid)
                {
                    if (_context.Fridges.Any(m => m.Name == fridge.Name && m.FridgeBrandId == fridge.FridgeBrandId))
                    {
                        if (fridge.Name != editValue)
                        {
                            ModelState.AddModelError("Name", "The Name is Already Exist");
                            fridge.Name = editValue;
                            return View(fridge);
                        }
                    }

                    _context.Update(fridge);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }

                return View(fridge);
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }
        }


        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fridge = await _context.Fridges
                .Include(t => t.FridgeBrand)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (fridge == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            return View(fridge);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var fridge = await _context.Fridges.FindAsync(id);
                _context.Fridges.Remove(fridge);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (DbUpdateException)
            {

                string errorMsg = "Fridge cannot be deleted. Because It is used in other Place (Ex. Outlet). If you really want to Delete, Please remove those first which use this Fridge and Try Again";
                return RedirectToAction("Delete", "Error", new { errorMsg = errorMsg });
            }

        }
    }
}
