﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IceCreamPOS.ViewModels
{
    public class StockVm
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public string ProductType { get; set; }
        public int StockQuantity { get; set; }
        public double Price { get; set; }
        public double TotalPrice { get; set; }

    }
}
