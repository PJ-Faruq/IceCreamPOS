﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IceCreamPOS.ViewModels
{
    public class OutletVm
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }

        public string FridgeName { get; set; }

        public string FridgeIssueDate { get; set; }

        public string OutletType { get; set; }
    }
}
