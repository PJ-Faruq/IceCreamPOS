﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IceCreamPOS.ViewModels
{
    public class CustomerProfileSearchVm
    {
        public int EnterpriseId { get; set; }
        public int? OutletId { get; set; }
        public DateTime Month { get; set; }
        public string AmountType { get; set; }
    }
}
