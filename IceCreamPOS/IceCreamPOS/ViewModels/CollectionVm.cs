﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IceCreamPOS.ViewModels
{
    public class CollectionVm
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Select an Outlet")]

        public int OutletId { get; set; }

        [Required(ErrorMessage = "Select an Enterprise")]
        public int EnterpriseId { get; set; }

        [Required(ErrorMessage = "Enter New Collection Amount")]
        public decimal NewCollection { get; set; }

        [Required(ErrorMessage = "Please Enter Total Due")]
        public decimal TotalDue { get; set; }

        [Required(ErrorMessage = "Select a Date")]
        public DateTime Date { get; set; }

        public string Particular { get; set; }


    }
}
