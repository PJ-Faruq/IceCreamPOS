﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IceCreamPOS.ViewModels
{
    public static class Common
    {
        public static List<AmountTypeVm>  GetAmountType()
        {
            List<AmountTypeVm> listOfType = new List<AmountTypeVm>()
            {
                new AmountTypeVm()
                {
                    Id=1,
                    Name="Due"
                },
                new AmountTypeVm()
                {
                    Id=2,
                    Name="Full Paid"
                }
            };

            return listOfType;
        }
    }
}
