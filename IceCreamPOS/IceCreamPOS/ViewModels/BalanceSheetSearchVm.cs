﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IceCreamPOS.ViewModels
{
    public class BalanceSheetSearchVm
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int EnterpriseId { get; set; }
    }
}
