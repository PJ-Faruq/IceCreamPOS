﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IceCreamPOS.ViewModels
{
    public class BadGoodSearchVm
    {
        public int Id { get; set; }
        public int EnterpriseId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}
