﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IceCreamPOS.ViewModels
{
    public class BalanceSheetVM
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string Particular { get; set; }
        public string VoucherNo { get; set; }
        public decimal Invoice { get; set; }
        public decimal Deposite { get; set; }
        public decimal Balance { get; set; }
        public decimal Profit { get; set; }

    }
}
