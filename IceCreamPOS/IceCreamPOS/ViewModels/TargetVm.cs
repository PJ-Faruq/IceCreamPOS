﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IceCreamPOS.ViewModels
{
    public class TargetVm
    {
        public decimal Target { get; set; }
        public decimal Achievement { get; set; }
        public decimal Percentage { get; set; }
    }
}
