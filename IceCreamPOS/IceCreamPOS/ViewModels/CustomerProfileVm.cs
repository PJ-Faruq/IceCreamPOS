﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IceCreamPOS.ViewModels
{
    public class CustomerProfileVm
    {
        public int Id { get; set; }

        [Display(Name = "Enterprise")]
        public int EnterpriseId { get; set; }

        [Display(Name = "Outlet")]
        public string OutletName { get; set; }
        public int? OutletId { get; set; }
        public DateTime Month { get; set; }

        public decimal TotalAmount { get; set; }
        public decimal TotalCollection { get; set; }
        public decimal Due { get; set; }

        public decimal TotalDue { get; set; }



    }
}
