﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IceCreamPOS.Models
{
    public class BadGoodsReturn
    {
        public int Id { get; set; }
        public int EnterpriseId { get; set; }
        public DateTime Date { get; set; }
        public decimal TotalAmount { get; set; }
        public virtual Enterprise Enterprise { get; set; }

        public List<BadGoodsReturnDetails> BadGoodsReturnDetails { get; set; }

    }
}
