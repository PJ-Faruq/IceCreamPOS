﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IceCreamPOS.Models
{
    public class PromotionalOffer
    {
        public int Id { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Display(Name = "Retail Discount")]
        public float? RetailDiscount { get; set; } = 0;

        [Display(Name = "Distributor Incentive")]
        public float? DistributorIncentive { get; set; } = 0;

        [Display(Name = "Distributor Additional Incentive")]
        public float? DistributorAdditionalIncentive { get; set; } = 0;

        [Display(Name = "DSR Incentive")]
        public float? DSRIncentive { get; set; } = 0;

    }
}
