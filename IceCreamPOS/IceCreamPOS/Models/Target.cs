﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IceCreamPOS.Models
{
    public class Target
    {
        public int Id { get; set; }

        [Required(ErrorMessage ="Please Enter Target Amount")]
        public decimal TargetAmount { get; set; }

        [Required(ErrorMessage = "Please Select a Month")]
        public DateTime Month { get; set; }

        [Required(ErrorMessage = "Please Select an Enterprise")]
        public int EnterpriseId { get; set; }

        [Required(ErrorMessage = "Please Select Food Type")]
        public int FoodTypeId { get; set; }
        public FoodType FoodType { get; set; }
        public Enterprise Enterprise { get; set; }
    }
}
