﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IceCreamPOS.Models
{
    public class Collection
    {
        public int Id { get; set; }
        public int OutletId { get; set; }

        public int EnterpriseId { get; set; }
        public DateTime Date { get; set; }
        public decimal CollectionAmount { get; set; }
        public string Particular { get; set; }
        public virtual Outlet Outlet { get; set; }
        public virtual Enterprise Enterprise { get; set; }
    }
}
