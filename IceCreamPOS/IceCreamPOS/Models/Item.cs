﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IceCreamPOS.Models
{
    public class Item
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is Required")]
        [StringLength(maximumLength: 50, ErrorMessage = "Name Should be in Between 2 and 50 Characters", MinimumLength = 2)]
        public string Name { get; set; }

        [Required(ErrorMessage ="Please Select Food Type")]
        [Display(Name ="Food Type")]
        public int FoodTypeId { get; set; }


        [Required(ErrorMessage ="Please Enter Piece Per Carton")]
        [Display(Name = "Piece Per Carton")]
        public int PiecePerCarton { get; set; }


        [Required(ErrorMessage = "Sales Price is Required")]
        [Display(Name = "Sales Price")]
        public double SalesPrice { get; set; }

        [Required(ErrorMessage = "Purchase Price is Required")]
        [Display(Name = "Purchase Price")]
        public double PurchasePrice { get; set; }

        [Required]
        public bool IsDeleted { get; set; }

        public virtual FoodType FoodType { get; set; }
    }
}
