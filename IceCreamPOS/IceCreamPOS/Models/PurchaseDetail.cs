﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IceCreamPOS.Models
{
    public class PurchaseDetail
    {
        public int Id { get; set; }
        [Display(Name = "Item")]
        public int ItemId { get; set; }

        [Required]
        public int Quantity { get; set; }


        [Required]
        public decimal UnitPrice { get; set; }

        [Required]
        public decimal PiecePerCarton { get; set; }

        [Required]
        public decimal TotalPrice { get; set; }
        public bool IsDeleted { get; set; }

        public int PurchaseId { get; set; }
        public virtual Purchase Purchase { get; set; }
        public virtual Item Item { get; set; }
    }
}
