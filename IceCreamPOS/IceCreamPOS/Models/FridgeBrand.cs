﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IceCreamPOS.Models
{
    public class FridgeBrand
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public virtual List<Fridge> Fridges { get; set; }
    }
}
