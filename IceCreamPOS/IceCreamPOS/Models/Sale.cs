﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IceCreamPOS.Models
{
    public class Sale
    {
        public int Id { get; set; }

        public string SaleNumber { get; set; }

        [Required(ErrorMessage = "Select an Enterprise")]
        [Display(Name = "Enterprise Name")]
        public int EnterpriseId { get; set; }

        [Required(ErrorMessage ="Please Select an Outlet")]
        [Display(Name = "Outlet")]
        public int OutletId { get; set; }


        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [Display(Name = "Sales Date")]
        public DateTime SalesDate { get; set; }

        public decimal SubTotal { get; set; }

        [Required]
        public decimal Discount { get; set; }

        [Required(ErrorMessage = "Total Amount in Required")]
        public decimal TotalAmount { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "Only positive number allowed")]
        public decimal TotalCollection { get; set; }
        public decimal DueAmount { get; set; }

        public virtual Outlet Outlet { get; set; }
        public virtual Enterprise Enterprise { get; set; }
        public virtual List<SalesDetail> SalesDetails { get; set; }


        public bool IsDeleted { get; set; }
    }
}
