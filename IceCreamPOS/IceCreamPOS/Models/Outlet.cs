﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IceCreamPOS.Models
{
    public class Outlet
    {
        public int Id { get; set; }

        [Required(ErrorMessage ="Please Enter Name of the Outlet")]
        public string Name { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }

        [Display(Name="Fridge")]
        public int? FridgeId { get; set; }
        public virtual Fridge Fridge { get; set; }

        [Display(Name = "Fridge Issue Date")]
        public DateTime? FridgeIssueDate { get; set; }

        [Required(ErrorMessage ="Please Select Outlet Type")]
        [Display(Name="Outlet Type")]
        public int OutletTypeId { get; set; }
        public virtual OutletType OutletType { get; set; }

    }
}
