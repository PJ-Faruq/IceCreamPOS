﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IceCreamPOS.Models
{
    public class Deposite
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please Select an Enterprise")]
        public int EnterpriseId { get; set; }

        [Required(ErrorMessage ="Please Select Deposite Date")]
        public DateTime Date { get; set; }
        public string Particular { get; set; }

        [Required(ErrorMessage = "Please Enter Deposite Amount")]
        public decimal Amount { get; set; }

        public virtual Enterprise Enterprise { get; set; }

    }
}
