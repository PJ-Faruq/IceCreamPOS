﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IceCreamPOS.Models
{
    public class BadGoodsReturnDetails
    {
        public int Id { get; set; }
        public int ItemId { get; set; }
        [Required]
        public int Quantity { get; set; }
        [Required]
        public decimal UnitPrice { get; set; }
        [Required]
        public decimal TotalPrice { get; set; }
        public int BadGoodsReturnId { get; set; }

        public virtual Item Item { get; set; }
        public virtual BadGoodsReturn BadGoodsReturn { get; set; } 
    }
}
