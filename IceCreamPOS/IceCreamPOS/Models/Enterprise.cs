﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IceCreamPOS.Models
{
    public class Enterprise
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual List<Sale> Sales { get; set; }
    }
}
