﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IceCreamPOS.Models
{
    public class Stock
    {
        public int Id { get; set; }
        public int ItemId { get; set; }
        public int EnterpriseId { get; set; }
        public int StockQuantity { get; set; }
        public virtual Item Item { get; set; }
        public virtual Enterprise Enterprise { get; set; }

    }
}
