﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IceCreamPOS.Models
{
    public class Purchase
    {
        public int Id { get; set; }

        public string PurchaseNumber { get; set; }

        [Required(ErrorMessage ="Select an Enterprise")]
        [Display(Name ="Enterprise Name")]
        public int EnterpriseId { get; set; }


        [Required]
        [Display(Name ="Purchase Date")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime PurchaseDate { get; set; }

        public decimal SubTotal { get; set; }

        [Required]
        public decimal Discount { get; set; }

        [Required]
        public decimal TotalAmount { get; set; }

        public bool IsDeleted { get; set; }

        public virtual Enterprise Enterprise { get; set; }
        public virtual List<PurchaseDetail> PurchaseDetails { get; set; }


    }
}
