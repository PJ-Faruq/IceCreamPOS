﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IceCreamPOS.Models;
using IceCreamPOS.ViewModels;

namespace IceCreamPOS.Models
{
    public class IceCreamDbContext : DbContext
    {
        public IceCreamDbContext(DbContextOptions<IceCreamDbContext> options) : base(options) { }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            //Remove Foreign Key Restriction
            foreach (var foreignKey in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                foreignKey.DeleteBehavior = DeleteBehavior.Restrict;
            }

            modelBuilder.Entity<Enterprise>().HasData(
                new Enterprise()
                {
                    Id=1,
                    Name = "L.R Enterprise"
                },
                new Enterprise()
                {
                    Id=2,
                    Name = "Toufiq Enterprise"
                }
                );
            modelBuilder.Entity<FoodType>().HasData(
                new FoodType()
                {
                    Id=1,
                    Name = "Ice Cream"
                },
                new FoodType()
                {
                    Id=2,
                    Name = "Frozen Food"
                }
                );

            modelBuilder.Entity<OutletType>().HasData(
                new OutletType()
                {
                    Id = 1,
                    Name = "Use Fridge"
                },
                new OutletType()
                {
                    Id = 2,
                    Name = "Others"
                }
                );
        }

        public DbSet<Item> Items { get; set; }

        public DbSet<Purchase> Purchase { get; set; }
        public DbSet<PurchaseDetail> PurchaseDetails { get; set; }
        public DbSet<Enterprise> Enterprises { get; set; }
        public DbSet<FoodType> FoodTypes { get; set; }
        public DbSet<FridgeBrand> FridgeBrands { get; set; }
        public DbSet<Fridge> Fridges { get; set; }
        public DbSet<Stock> Stocks { get; set; }
        public DbSet<Outlet> Outlets { get; set; }
        public DbSet<OutletType> OutletTypes { get; set; }
        public DbSet<Sale> Sales { get; set; }
        public DbSet<SalesDetail> SalesDetails { get; set; }
        public DbSet<Deposite> Deposites { get; set; }
        public DbSet<Target> Target { get; set; }
        public DbSet<PromotionalOffer> PromotionalOffer { get; set; }
        public DbSet<BadGoodsReturn> BadGoodsReturn { get; set; }
        public DbSet<BadGoodsReturnDetails> BadGoodsReturnDetails { get; set; }
        public DbSet<Collection> Collection { get; set; }
        public DbSet<IceCreamPOS.ViewModels.CustomerProfileVm> CustomerProfileVm { get; set; }

    }
}
