﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IceCreamPOS.Models
{
    public class OutletType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
