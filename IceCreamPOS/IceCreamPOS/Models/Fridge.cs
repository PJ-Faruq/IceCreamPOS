﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IceCreamPOS.Models
{
    public class Fridge
    {
        public int Id { get; set; }

        [Required(ErrorMessage ="Fridge Name is Required")]
        public string Name{ get; set; }
        public string Type{ get; set; }

        [Required(ErrorMessage = "Model Number is Required")]
        public string ModelNumber { get; set; }

        [Required(ErrorMessage = "Please Select Brand")]
        [Display(Name ="Brand")]
        public int FridgeBrandId { get; set; }
        public FridgeBrand FridgeBrand { get; set; }
    }
}
