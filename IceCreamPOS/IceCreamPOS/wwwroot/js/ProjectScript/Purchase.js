﻿
$(document).ready(function () {
    $("#PurchaseDate").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd MM, yy'
    });


})

$("#btnAdd").on("click", function () {


    var itemId = $("#ItemId").val();
    var itemName = $("#ItemId option:selected").text();
    var quantity = $("#Quantity").val();

    if (itemId == "" || itemId == undefined) {
        toastr.error('Please Select a Product');
        return;
    }

    if (quantity == "" || quantity == undefined) {
        toastr.error('Please Enter Quantity');
        return;
    }


    var piecePerCarton = $("#PiecePerCarton").val();
    var UnitPrice = $("#UnitPrice").val();
    var TotalPrice = mathRound($("#UnitPrice").val() * quantity);

    var index = $("#PurchaseItemList").children("tr").length;

    var indexCell = "<td>" + index + "</td>";
    var itemCell = "<td id='" + itemId + "'>" + itemName + "</td>";
    var quantityCell = "<td>" + quantity + "</td>";
    var piecePerCartonCell = "<td>" + piecePerCarton + "</td>";
    var unitPriceCell = "<td>" + UnitPrice + "</td>";
    var totalPriceCell = "<td>" + TotalPrice + "</td>";
    var remove = "<td> <a class='btn btn-danger btn - sm' href='#' onclick='DeleteRow(" + index + ")'><i class='fas fa-trash'></i> Remove</a > <td>";



    $("#PurchaseItemList").append("<tr id='DelRow_" + index + "'>'" + indexCell + itemCell + quantityCell + unitPriceCell + piecePerCartonCell + totalPriceCell + remove + "'</tr>");

    maintainSerial();
    subTotal();
    clearValue();
})

function clearValue() {
    $("#ItemId").val("");
    $("#Quantity").val("");
    $("#PiecePerCarton").val("");
    $("#UnitPrice").val("");
}

function maintainSerial() {
    $("#PurchaseItemList tr").each(function (index, value) {
        count = index + 1;
        $(this).children(":first").text(count);
    });

}

function DeleteRow(index) {
    $("#DelRow_" + index).remove();
    maintainSerial();
    subTotal();

}

function onSubmit() {

    var rowCount = $("#PurchaseItemList").children("tr").length;
    if (rowCount <= 0) {
        toastr.error('Please Add at least one Product');
        return;
    }
    var purchaseNumber = $("#PurchaseNumber").val();
    var enterpriseId = $("#EnterpriseId").val();

    if (enterpriseId == "" || enterpriseId == undefined) {
        toastr.error('Select an Enterprise');
        return;
    }

    var purchaseDate = $("#PurchaseDate").val();
    if (purchaseDate == "" || purchaseDate == undefined) {
        toastr.error('Select Purchase Date');
        return;
    }

    var discount = $("#Discount").val();
    if (discount == "" || discount == undefined) {
        toastr.error('Enter Discount');
        return;
    }

    var subTotal = $("#SubTotal").val();
    var discount = $("#Discount").val();
    var totalAmount = $("#TotalAmount").val();

    var purchaseDetail = [];

    $("#PurchaseItemList tr").each(function (index, value) {

        var itemId = $(this).children(":nth-child(2)").attr("id");
        var quantity = $(this).children(":nth-child(3)").text();
        var unitPrice = $(this).children(":nth-child(4)").text();
        var piecePerCarton = $(this).children(":nth-child(5)").text();
        var totalPrice = $(this).children(":nth-child(6)").text();

        var itemDetail = {
            "itemId": itemId,
            "quantity": quantity,
            "unitPrice": unitPrice,
            "piecePerCarton": piecePerCarton,
            "totalPrice": totalPrice
        }

        purchaseDetail.push(itemDetail);
    });

    console.log(purchaseDetail);

    var purchaseItems = {
        "enterpriseId": enterpriseId,
        "purchaseDate": purchaseDate,
        "subTotal": subTotal,
        "discount": discount,
        "totalAmount": totalAmount,
        "purchaseNumber": purchaseNumber,
        "purchaseDetails": purchaseDetail
    }

    $.ajax({
        url: "/Purchases/Create",
        data: { purchase: purchaseItems },
        type: "post",
        success: function (response) {
            console.log(response);
            if (response.status =="Yes") {

                $("#PurchaseItemList tr").remove();
                $("#PurchaseDate").val("");
                $("#EnterpriseId").val("");
                $("#FoodTypeId").val("");
                $("#ItemId").empty();
                $("#SubTotal").val("");
                $("#Discount").val("");
                $("#TotalAmount").val("");
                clearValue();
                $("#PurchaseNumber").val(response.code);
                toastr.success("Products are Purchased Successfully");

            }
            else {
                $("#PurchaseNumber").val(response.code);
                toastr.error("Purchase Product Failed !!");
            }
        }

    });


}

$("#FoodTypeId").on("change", function () {
    var foodTypeId = $(this).val();

    $.ajax({
        url: "/Purchases/GetItemListByFoodType",
        data: { foodTypeId: foodTypeId },
        type: "post",
        success: function (response) {
            console.log(response);
            if (response != null) {
                $("#ItemId").empty();
                if (response.length > 0) {
                    $("#ItemId").append("<option value=''>" + "--Select--" + "</option>");
                    $.each(response, function (index, value) {
                        $("#ItemId").append("<option value='" + value.id + "'>" + value.name + "</option>");
                    });
                }

            }
            else {
                $("#ItemId").append("<option value=''>" + "Select Product" + "</option>");
                $("#ItemId").empty();
            }


        }

    });

})

$("#ItemId").on("change", function () {
    var itemId = $(this).val();

    $.ajax({
        url: "/Purchases/GetItemById",
        data: { itemId: itemId },
        type: "post",
        success: function (response) {

            if (response != null) {
                $("#UnitPrice").val(response.purchasePrice);
                $("#PiecePerCarton").val(response.piecePerCarton);
            }
            else {
                $("#UnitPrice").val("");
            }


        }

    });

})

function subTotal() {
    var sumOfTotal = 0;
    if ($("#PurchaseItemList").children("tr").length == 0) {
        $("#SubTotal").val(0);
    }
    else {
        $("#PurchaseItemList tr ").each(function (index, value) {
            var totalPrice = parseFloat($(this).children(":nth-child(6)").text());
            sumOfTotal = mathRound(sumOfTotal + totalPrice);
            $("#SubTotal").val(sumOfTotal);

        });
    }

}

$("#Discount").on("change", function () {
    var discount = parseFloat($(this).val());
    var subTotal = parseFloat( $("#SubTotal").val());
    var totalAmount = subTotal-(subTotal * discount) / 100;
    $("#TotalAmount").val(Math.round((totalAmount) * 100) / 100);

})
