﻿
$(document).ready(function () {
    $("#SalesDate").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd MM, yy'
    });
    var stock = 0;

})

$("#btnAdd").on("click", function () {


    var foodTypeId = $("#FoodTypeId").val();
    var itemId = $("#ItemId").val();
    var itemName = $("#ItemId option:selected").text();
    var quantity = $("#Quantity").val();

    if (foodTypeId == null || foodTypeId == "" || foodTypeId == undefined) {
        toastr.error("Please Select Product Type");
        return;
    }

    if (itemId == "" || itemId == undefined) {
        toastr.error('Please Select a Product');
        return;
    }

    if (quantity == "" || quantity == undefined) {
        toastr.error('Please Enter Quantity');
        return;
    }

    if (quantity <= 0) {
        toastr.error('Minimum Quantity Should be 1');
        return;
    }

    if (quantity > stock) {
        toastr.error('Out of Stock');
        return;
    }

    // Check if product is already added
    if ($("#SalesItemList").children("tr").length > 0) {
        var isItemExist = false;
        var itemId = $("#ItemId").val();
        $("#SalesItemList tr").each(function (index, value) {
            var itemIdTable = $(this).children(":nth-child(2)").text();
            if (itemIdTable == itemId) {
                isItemExist = true;
            }
        });

        if (isItemExist) {
            toastr.error('Product is already Added');
            isItemExist = false;
            return;
        }
    }



    var piecePerCarton = $("#PiecePerCarton").val();
    var UnitPrice = $("#UnitPrice").val();
    var TotalPrice = mathRound($("#UnitPrice").val() * quantity);

    var index = $("#SalesItemList").children("tr").length;

    var indexCell = "<td>" + index + "</td>";
    var itemCell = "<td id='" + itemId + "'>" + itemName + "</td>";
    var quantityCell = "<td>" + quantity + "</td>";
    var piecePerCartonCell = "<td>" + piecePerCarton + "</td>";
    var unitPriceCell = "<td>" + UnitPrice + "</td>";
    var totalPriceCell = "<td>" + TotalPrice + "</td>";
    var remove = "<td> <a class='btn btn-danger btn - sm' href='#' onclick='DeleteRow(" + index + ")'><i class='fas fa-trash'></i> Remove</a > <td>";


    $("#SalesItemList").append("<tr id='DelRow_" + index + "'>'" + indexCell + itemCell + quantityCell + unitPriceCell + piecePerCartonCell + totalPriceCell + remove + "'</tr>");

    maintainSerial();
    subTotal();
    clearValue();
})

function isAlreadyAdded() {



}

function clearValue() {
    $("#ItemId").val("");
    $("#Quantity").val("");
    $("#PiecePerCarton").val("");
    $("#UnitPrice").val("");
    $("#Stock").val("");

}

function maintainSerial() {
    $("#SalesItemList tr").each(function (index, value) {
        count = index + 1;
        $(this).children(":first").text(count);
    });

}

function DeleteRow(index) {
    $("#DelRow_" + index).remove();
    maintainSerial();
    subTotal();

}

function onSubmit() {

    var rowCount = $("#SalesItemList").children("tr").length;
    if (rowCount <= 0) {
        toastr.error('Please Add at least one Product');
        return;
    }

    var enterpriseId = $("#EnterpriseId").val();
    var saleNumber = $("#SaleNumber").val();

    if (enterpriseId == "" || enterpriseId == undefined) {
        toastr.error('Select an Enterprise');
        return;
    }

    var salesDate = $("#SalesDate").val();
    if (salesDate == "" || salesDate == undefined) {
        toastr.error('Select Sales Date');
        return;
    }


    var outletId = $("#OutletId").val();

    if (outletId == "" || outletId == undefined) {
        toastr.error('Select an Outlet');
        return;
    }

    var subTotal = $("#SubTotal").val();
    var discount = $("#Discount").val();
    var totalAmount = $("#TotalAmount").val();

    if (discount == "" || discount == undefined) {
        toastr.error('Enter Discount');
        return;
    }

    var totalCollection = $("#TotalCollection").val();
    if (totalCollection == "" || totalCollection == undefined) {
        toastr.error('Select Enter Total Collection');
        return;
    }

    if (totalCollection < 0) {
        toastr.error('Total Collection should be Positive number');
        return;
    }

    if (parseFloat(totalCollection) > parseFloat(totalAmount)) {
        toastr.error('Collection Amount should not be greater than Total Amount');
        return;
    }

    var dueAmount = $("#DueAmount").val();

    var salesDetail = [];

    $("#SalesItemList tr").each(function (index, value) {

        var itemId = $(this).children(":nth-child(2)").attr("id");
        var quantity = $(this).children(":nth-child(3)").text();
        var unitPrice = $(this).children(":nth-child(4)").text();
        var piecePerCarton = $(this).children(":nth-child(5)").text();
        var totalPrice = $(this).children(":nth-child(6)").text();

        var itemDetail = {
            "itemId": itemId,
            "quantity": quantity,
            "unitPrice": unitPrice,
            "piecePerCarton": piecePerCarton,
            "totalPrice": totalPrice
        }

        salesDetail.push(itemDetail);
    });

    console.log(salesDetail);

    var salesItems = {
        "enterpriseId": enterpriseId,
        "saleNumber": saleNumber,
        "outletId": outletId,
        "salesDate": salesDate,
        "subTotal": subTotal,
        "totalAmount": totalAmount,
        "discount": discount,
        "totalCollection": totalCollection,
        "dueAmount": dueAmount,
        "salesDetails": salesDetail
    }

    $.ajax({
        url: "/Sales/Create",
        data: { sale: salesItems },
        type: "post",
        success: function (response) {
            console.log(response);
            if (response.status == "Yes") {

                $("#SalesItemList tr").remove();
                $("#SalesDate").val("");
                $("#EnterpriseId").val("");
                $("#OutletId").val("");
                $("#SubTotal").val("");
                $("#TotalAmount").val("");
                $("#Discount").val("");
                $("#TotalCollection").val("");
                $("#DueAmount").val("");
                $("#SaleNumber").val(response.code);
                toastr.success("Sale Record Successfully Added");

            }
            else {
                $("#SaleNumber").val(response.code);
                toastr.error("Sale Record Added Failed !");
            }
        }

    });


}


$("#FoodTypeId").on("change", function () {
    var foodTypeId = $(this).val();
    if (foodTypeId == null || foodTypeId == "" || foodTypeId == undefined) {
        clearValue();
        return;
    }

    $.ajax({
        url: "/Purchases/GetItemListByFoodType",
        data: { foodTypeId: foodTypeId },
        type: "post",
        success: function (response) {
            console.log(response);
            if (response != null) {
                $("#ItemId").empty();
                if (response.length > 0) {
                    $("#ItemId").append("<option value=''>" + "--Select Item--" + "</option>");
                    $.each(response, function (index, value) {
                        $("#ItemId").append("<option value='" + value.id + "'>" + value.name + "</option>");
                    });
                }

            }
            else {
                $("#ItemId").append("<option value=''>" + "Select Product" + "</option>");
                $("#ItemId").empty();
            }


        }

    });

})

$("#ItemId").on("change", function () {

    var itemId = $(this).val();
    var enterpriseId = $("#EnterpriseId").val();

    if (enterpriseId == null || enterpriseId == "" || enterpriseId == undefined) {
        toastr.error("Please Select an Enterprise First");
        $("#ItemId").val("");
        return;
    }

    if (itemId == null || itemId == "" || itemId == undefined) {
        $("#PiecePerCarton").val("");
        $("#UnitPrice").val("");
        $("#Stock").val("");
        return;
    }

    $.ajax({
        url: "/Sales/GetItemById",
        data: { itemId: itemId, enterpriseId: enterpriseId },
        type: "post",
        success: function (response) {
            console.log(response);
            if (response != null) {
                $("#PiecePerCarton").val(response.piecePerCarton);
                $("#UnitPrice").val(response.salesPrice);
                $("#Stock").val(response.stockQuantity);
                stock = response.stockQuantity;

            }
            else {
                $("#PiecePerCarton").val("");
                $("#UnitPrice").val("");
            }


        }

    });

})


$("#EnterpriseId").on("change", function () {

     $("#FoodTypeId").val("");

})

function subTotal() {
    var sumOfTotal = 0;
    if ($("#SalesItemList").children("tr").length == 0) {
        $("#SubTotal").val(0);
    }
    else {
        $("#SalesItemList tr ").each(function (index, value) {
            var totalPrice = parseFloat($(this).children(":nth-child(6)").text());
            sumOfTotal = sumOfTotal + totalPrice;
            $("#SubTotal").val(sumOfTotal);

        });
    }

}

$("#TotalCollection").on("change", function () {

    var totalCollection = $("#TotalCollection").val();
    var totalAmount = $("#TotalAmount").val();

    if (totalCollection != "" || totalCollection != undefined) {

        dueAmount = totalAmount - totalCollection;
        $("#DueAmount").val(Math.round((dueAmount) * 100) / 100);
    }



})

$("#Discount").on("change", function () {
    var discount = parseFloat($(this).val());
    var subTotal = parseFloat($("#SubTotal").val());

    var totalAmount = subTotal - (subTotal * discount) / 100;

    $("#TotalAmount").val(Math.round((totalAmount) * 100) / 100);

})