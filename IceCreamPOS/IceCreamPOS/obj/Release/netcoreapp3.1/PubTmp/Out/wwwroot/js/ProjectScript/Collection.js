﻿$(document).ready(function () {
    $("#Date").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd MM, yy'
    });


});

$("#OutletId").on("change", function () {
    getDueAmount();

})

$("#EnterpriseId").on("change", function () {

    getDueAmount();
})

function getDueAmount() {


    var outletId = $("#OutletId").val();
    var enterpriseId = $("#EnterpriseId").val();

    if (outletId != "" && enterpriseId != "") {
        $.ajax({
            url: "/Collections/GetCollectionByOutletId",
            data: { outletId: outletId, enterpriseId: enterpriseId },
            type: "post",
            success: function (response) {

                if (response != null) {
                    $("#TotalDue").val(valueWithCommas(response));
                }
                else {

                }


            }

        });
    }

    
}

