﻿$(document).ready(function () {
    $("#FromDate").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd MM, yy'
    });

    $("#ToDate").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd MM, yy'
    });



})

$("#FromDate").on("change", function () {
    var date =new Date( $(this).val());
    $("#fromDateShow").html(getCustomizeDate(date));
});

$("#ToDate").on("change", function () {
    var date = new Date($(this).val());
    $("#toDateShow").html(getCustomizeDate(date));
});

function getCustomizeDate(date) {
    const monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];

    var monthName = monthNames[date.getMonth()];
    var day = date.getDate();
    var year = date.getFullYear();

    var newDate = day + " " + monthName + ", " + year;
    return newDate;
}

$("#btnSubmit").on("click", function (e) {

    var enterpriseId = $("#EnterpriseId").val();

    if (enterpriseId == null || enterpriseId == "" || enterpriseId == undefined) {
        toastr.error("Please Select an Enterprise");
        e.preventDefault();
        return;
    }

    var fromDate = $("#FromDate").val();
    
    

    if (fromDate == null || fromDate == "" || fromDate == undefined) {
        toastr.error("Please Select FromDate");
        e.preventDefault();
        return;
    }

    var toDate = $("#ToDate").val();

    if (toDate == null || toDate == "" || toDate == undefined) {
        toastr.error("Please Select ToDate");
        e.preventDefault();
        return;
    }



    //var data = {
    //    "fromDate": fromDate,
    //    "toDate": toDate,
    //    "enterpriseId": enterpriseId
    //}

    //$.ajax({
    //    url: "/Report/BalanceSheet",
    //    type: "post",
    //    data: { searchVm: data },
    //    success: function (response) {
    //        if (response != null) {
    //            console.log(response.length);
    //            createTableRow(response);
    //        }
    //    }
    //})
})

function createTableRow(response) {

    //Various kind of date format
    //var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    //var today = new Date();

    //console.log(today.toLocaleDateString("en-US")); // 9/17/2016
    //console.log(today.toLocaleDateString("en-US", options)); // Saturday, September 17, 2016
    //console.log(today.toLocaleDateString("hi-IN", options));

    $("#tblBalanceSheet tr").remove();

    response.forEach(function (value, index) {

        var date = new Date(value.date);
        var newDate = date.getDate() + "/" + date.getMonth()+"/"+ date.getFullYear();

        var dateCell = "<td>" + newDate + "</td>";
        var particularCell = "<td>" + value.particular + "</td>";
        var voucherNoCell = "<td>" + value.voucherNo + "</td>";
        var invoiceCell = "<td>" + value.invoice + "</td>";
        var depositeCell = "<td>" + value.deposite + "</td>";
        var balanceCell = "<td>" + value.balance + "</td>";
        var profitCell = "<td>" + value.profit + "</td>";

        //$("#tblBalanceSheet").append("<tr" + dateCell + particularCell + voucherNoCell + invoiceCell + depositeCell + balanceCell + profitCell + "'</tr>");
        $("#tblBalanceSheet").append("<tr>'"+dateCell + particularCell + voucherNoCell + invoiceCell + depositeCell + balanceCell + profitCell+"'</tr>");
    })


}