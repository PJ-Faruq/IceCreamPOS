﻿
$(document).ready(function () {
    $("#Date").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd MM, yy'
    });


})

$("#btnAdd").on("click", function () {


    var itemId = $("#ItemId").val();
    var itemName = $("#ItemId option:selected").text();
    var quantity = $("#Quantity").val();

    if (itemId == "" || itemId == undefined) {
        toastr.error('Please Select a Product');
        return;
    }

    if (quantity == "" || quantity == undefined) {
        toastr.error('Please Enter Quantity');
        return;
    }

    var UnitPrice = parseFloat($("#UnitPrice").val());
    var TotalPrice =Math.round((parseFloat($("#UnitPrice").val() * quantity)) * 100) / 100;

    var index = $("#BadGoodsList").children("tr").length;

    var indexCell = "<td>" + index + "</td>";
    var itemCell = "<td id='" + itemId + "'>" + itemName + "</td>";
    var quantityCell = "<td>" + quantity + "</td>";
    var unitPriceCell = "<td>" + UnitPrice + "</td>";
    var totalPriceCell = "<td>" + TotalPrice + "</td>";
    var remove = "<td> <a class='btn btn-danger btn - sm' href='#' onclick='DeleteRow(" + index + ")'><i class='fas fa-trash'></i> Remove</a > <td>";



    $("#BadGoodsList").append("<tr id='DelRow_" + index + "'>'" + indexCell + itemCell + quantityCell + unitPriceCell + totalPriceCell + remove + "</tr>");

    maintainSerial();
    totalAmount();
    clearValue();
})

function clearValue() {
    $("#ItemId").val("");
    $("#Quantity").val("");
    $("#PiecePerCarton").val("");
    $("#UnitPrice").val("");
}

function maintainSerial() {
    $("#BadGoodsList tr").each(function (index, value) {
        count = index + 1;
        $(this).children(":first").text(count);
    });

}

function DeleteRow(index) {
    $("#DelRow_" + index).remove();
    maintainSerial();
    totalAmount();

}

function onSubmit() {

    var rowCount = $("#BadGoodsList").children("tr").length;
    if (rowCount <= 0) {
        toastr.error('Please Add at least one Product');
        return;
    }

    var enterpriseId = $("#EnterpriseId").val();
    if (enterpriseId == "" || enterpriseId == undefined) {
        toastr.error('Select an Enterprise');
        return;
    }

    var date = $("#Date").val();
    if (date == "" || date == undefined) {
        toastr.error('Select Purchase Date');
        return;
    }

    var totalAmount = $("#TotalAmount").val();

    var badGoodsReturnDetails = [];

    $("#BadGoodsList tr").each(function (index, value) {

        var itemId = $(this).children(":nth-child(2)").attr("id");
        var quantity = $(this).children(":nth-child(3)").text();
        var unitPrice = $(this).children(":nth-child(4)").text();
        var totalPrice = $(this).children(":nth-child(5)").text();

        var itemDetail = {
            "itemId": itemId,
            "quantity": quantity,
            "unitPrice": unitPrice,
            "totalPrice": totalPrice
        }

        badGoodsReturnDetails.push(itemDetail);
    });

    console.log(badGoodsReturnDetails);

    var badGoodsItems = {
        "enterpriseId": enterpriseId,
        "date": date,
        "totalAmount": totalAmount,
        "badGoodsReturnDetails": badGoodsReturnDetails
    }

    $.ajax({
        url: "/BadGoodsReturns/Create",
        data: { badGoodsReturn: badGoodsItems },
        type: "post",
        success: function (response) {
            if (response == 1) {

                $("#BadGoodsList tr").remove();
                $("#Date").val("");
                $("#EnterpriseId").val("");
                $("#ItemId").empty();
                $("#TotalAmount").val("");
                clearValue();
                toastr.success("Bad Goods Return Info. Successfully Added");

            }
            else {
                toastr.error("Bad Goods Return Info. Added Failed !!");
            }
        }

    });


}


$("#ItemId").on("change", function () {
    var itemId = $(this).val();

    $.ajax({
        url: "/Purchases/GetItemById",
        data: { itemId: itemId },
        type: "post",
        success: function (response) {

            if (response != null) {
                $("#PiecePerCarton").val(response.piecePerCarton);
                var unitPrice = Math.round((response.purchasePrice / response.piecePerCarton) * 100) / 100;
                $("#UnitPrice").val(unitPrice);
            }
            else {
                $("#PiecePerCarton").val("");
                $("#UnitPrice").val("");
            }


        }

    });

})

function totalAmount() {
    var sumOfTotal = 0;
    if ($("#BadGoodsList").children("tr").length == 0) {
        $("#TotalAmount").val(0);
    }
    else {
        $("#BadGoodsList tr ").each(function (index, value) {
            var totalPrice = parseFloat($(this).children(":nth-child(5)").text());
            sumOfTotal = sumOfTotal + totalPrice;
            $("#TotalAmount").val(sumOfTotal);

        });
    }

}

$("#Discount").on("keyup", function () {
    var discount = parseFloat($(this).val());
    var totalAmount = parseFloat($("#TotalAmount").val());

    var totalAmount = totalAmount - (totalAmount * discount) / 100;
    $("#TotalAmount").val(Math.round((totalAmount) * 100) / 100);

})